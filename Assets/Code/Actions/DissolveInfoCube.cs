﻿using UnityEngine;

public class DissolveInfoCube : ActionInfo
{
    public Renderer Renderer;
    public float TimeToDissolveCube;
}