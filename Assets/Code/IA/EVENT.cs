﻿namespace FSM
{
    public enum EVENT
    {
        ENTER,
        UPDATE,
        EXIT
    };
}