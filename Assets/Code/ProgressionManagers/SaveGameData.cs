﻿namespace Domain
{
    public class SaveGameData
    {
        public int LastLevelCompleted;

        public SaveGameData()
        {
            LastLevelCompleted = -1;
        }
    }
}