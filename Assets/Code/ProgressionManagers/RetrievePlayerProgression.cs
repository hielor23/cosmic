﻿using Domain;
using Domain.LoaderSaveGame;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class RetrievePlayerProgression : MonoBehaviour
{
    private IEventManager _eventManager;
    private ILoader _loader;

    private SaveGameData _saveGameData;

    public SaveGameData GameData => _saveGameData;

    void Awake()
    {
        _loader = ServiceLocator.Instance.GetService<ILoader>();
    }

    private void Start()
    {
        if (_loader.HasSavedGame())
        {
            _saveGameData = _loader.LoadGame();
            return;
        }
        _saveGameData = new SaveGameData();

    }

    private void OnDestroy()
    {
    }
}
