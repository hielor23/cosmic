﻿using Domain;
using Domain.SaverGame;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class UpdatePlayerProgression : MonoBehaviour
{
    private IEventManager _eventManager;
    private ISaver _saver;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _saver = ServiceLocator.Instance.GetService<ISaver>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }

    private void SavePlayerProgress(SavePlayerProgressSignal signal)
    {
        _saver.SaveGame(new SaveGameData()
        {
            LastLevelCompleted = signal.IdSceneToSave
        });
    }

    public void DeletePlayerProgress()
    {
        _saver.DeleteSaveGame();
    }
}