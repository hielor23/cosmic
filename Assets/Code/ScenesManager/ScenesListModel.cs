﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScenesListModel : MonoBehaviour
{
    [SerializeField] private List<SceneInfo> SceneInfos;

    public SceneInfo SceneInfoById(int id)
    {
        var sceneInfo = SceneInfos.Single(x => x.SceneId == id);
        return sceneInfo;
    }

    public SceneInfo SceneInfoByName(String sceneName)
    {
        var sceneInfo = SceneInfos.Single(x => x.SceneName == sceneName);
        return sceneInfo;
    }
    
    public SceneInfo GetNextScene(String sceneName)
    {
        int idCurrentScene = SceneInfoByName(sceneName).SceneId;
        if (idCurrentScene + 1 < SceneInfos.Count)
        {
            idCurrentScene++;
        }
        else
        {
            return new SceneInfo{SceneName = "Credits"};
        }

        return SceneInfoById(idCurrentScene);
    }

    public int GetSceneCount()
    {
        return SceneInfos.Count;
    }
}