﻿using System;
using DG.Tweening;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using VFX;

public class SceneChanger : MonoBehaviour
{
    [SerializeField] private ScenesListModel _scenesListModel;
    private IEventManager _eventManager;
    private Sequence _sequence;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void HandleCompletedLevel(PlayerHasCompletedLevelSignal obj)
    {
        _sequence = LevelEffect.Singleton.DissolveLevelEffect();
        _sequence.onComplete += ChangeLevel;
    }

    private void ChangeLevel()
    {
        _sequence.onComplete -= ChangeLevel;
        SceneInfo nextSceneInfo = _scenesListModel.GetNextScene(GetCurrentScene());
        SceneInfo currentSceneInfo = _scenesListModel.SceneInfoByName(GetCurrentScene());
        new SavePlayerProgressSignal()
        {
            IdSceneToSave = currentSceneInfo.SceneId
        }.Execute();
        Debug.Log("Has pasado de nivel");
        new PlayFadeLevelSoundSignal().Execute();
        SceneManager.LoadScene(nextSceneInfo.SceneName);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(GetCurrentScene());
    }

    public void GoToFirstLevel()
    {
        SceneManager.LoadScene(_scenesListModel.SceneInfoById(0).SceneName);
    }

    public void GoToInitScene()
    {
        SceneManager.LoadScene("Init");
    }

    private String GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadCurrentLevelFromInitScene(int gameDataLastLevelCompleted)
    {
        int nextLevel = gameDataLastLevelCompleted + 1;
        if (gameDataLastLevelCompleted + 1 > _scenesListModel.GetSceneCount())
        {
            nextLevel = gameDataLastLevelCompleted;
        }

        SceneManager.LoadScene(_scenesListModel.SceneInfoById(nextLevel).SceneName);
    }
}