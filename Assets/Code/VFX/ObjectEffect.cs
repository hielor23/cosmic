﻿using System;
using UnityEngine;
using DG.Tweening;

namespace VFX
{
    public class ObjectEffect : MonoBehaviour
    {
        public static ObjectEffect Singleton;

        [Header("Object Effects")] [SerializeField]
        private Effect changeColor = default;

        [SerializeField] private Effect dissolve = default;
        [SerializeField] private EffectParticle enemyDead = default;

        private void Awake()
        {
            if (!Singleton)
            {
                Singleton = this;
            }

            else
            {
                Destroy(gameObject);
                return;
            }
        }

        private void Start()
        {
            //Renderer renderer = GetComponent<Renderer>();
            //OneTimeEffect(renderer, dissolve);
            //OneTimeEffect(renderer, changeColor).OnComplete(() => { OneTimeEffect(renderer, dissolve); });
        }

        public Sequence OneTimeEffect(Renderer objectRenderer, Effect effect, float durationEffect)
        {
            Sequence sequence = DOTween.Sequence();

            objectRenderer.material = effect.EffectMaterial;
            objectRenderer.material.SetFloat(effect.ShaderPropertyName, 0);
            sequence.Join(objectRenderer.material.DOFloat(effect.TargetValue, effect.ShaderPropertyName,
                durationEffect));

            return sequence;
        }

        public void OneTimeEffect(Transform position, EffectParticle effect, float durationEffect)
        {
            GameObject instance = Instantiate(effect.Prefab, position.position, Quaternion.identity);
            TimerWithParameters timer = new TimerWithParameters();
            timer.SetTimeToWait(durationEffect);
            timer.OnTimerEnds += DestroyEffect;
            TimerParameters timerParameters = new TimerParameters();
            timerParameters.GameObject.Add(instance);
            StartCoroutine(timer.TimerCoroutine(timerParameters));
        }

        private void DestroyEffect(TimerParameters parameters)
        {
            parameters.Timer.OnTimerEnds -= DestroyEffect;
            foreach (var VARIABLE in parameters.GameObject)
            {
                Destroy(VARIABLE);
            }
        }

        public Effect ChangeColor
        {
            get { return changeColor; }
        }

        public Effect Dissolve
        {
            get { return dissolve; }
        }

        public EffectParticle EnemyDead
        {
            get { return enemyDead; }
        }
    }
}