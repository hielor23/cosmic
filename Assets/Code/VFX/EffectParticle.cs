﻿using UnityEngine;

namespace VFX
{
    [System.Serializable]
    public class EffectParticle
    {
        public GameObject Prefab;
    }
}