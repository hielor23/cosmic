﻿using System;
using InputPlayerSystem;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Utils.Input
{
    public class ReadInputPlayer : MonoBehaviour
    {
        private GameplayInput _playerInputActions;
        private Vector2 _movementInMenuInput;
        private Vector2 _mouseMovementInput;
        public Vector3 MovementInGameAxis { get; private set; }
        public Vector3 MouseMovement { get; private set; }

        public event Action OnPlayerPressedSpaceKey = delegate { };
        public event Action OnPlayerStartMoving = delegate { };

        public event Action OnPlayerPressLeftButtonMouse = delegate { };
        public event Action OnPlayerReleaseLeftButtonMouse = delegate { };
        public event Action OnDebugEscPressed = delegate { };


        private bool _isPaused;
        public event Action OnPlayerPressedControlKey = delegate { };
        public event Action OnPlayerReleaseControlKey = delegate { };

        public bool IsPaused
        {
            get { return _isPaused; }
        }

        private void OnEnable()
        {
            _playerInputActions?.Enable();
        }

        private void OnDisable()
        {
            _playerInputActions?.Disable();
        }

        private void Awake()
        {
            _playerInputActions = new GameplayInput();


            _playerInputActions.Gameplay.Jump.performed += ctx => PlayerPressSpace(); //PRESS ONE TIME
            // _playerInputActions.Gameplay.Interact.canceled += ctx => InteractReleased(); //PRESS AND RELEASE

            _playerInputActions.Gameplay.Movement.performed += PlayerMove;
            _playerInputActions.Gameplay.Movement.canceled += PlayerStopMoving;

            _playerInputActions.Gameplay.Crouch.performed += PlayerCrouch;
            _playerInputActions.Gameplay.Crouch.canceled += PlayerStopCrouch;
            
            // _playerInputActions.Gameplay.AttackRight.performed += ctx => AttackRight();
            //TODO Check cuando dejamos de pulsar
            _playerInputActions.Gameplay.Interact.performed += ctx => AttackLeft();
            _playerInputActions.Gameplay.Interact.canceled += ctx => UndoAttackLeft();

            _playerInputActions.Gameplay.PauseGame.performed += ctx => EscapeButtonPressed();

            _playerInputActions.Debug.EscapeKey.performed += ctx => DebugEscapeButtonPressed();
        }

        private void PlayerCrouch(InputAction.CallbackContext obj)
        {
            OnPlayerPressedControlKey.Invoke();
        }

        private void PlayerStopCrouch(InputAction.CallbackContext obj)
        {
            OnPlayerReleaseControlKey.Invoke();
        }

        private void OnDestroy()
        {
            if (_playerInputActions != null)
            {
                _playerInputActions.Gameplay.Jump.performed -= ctx => PlayerPressSpace(); //PRESS ONE TIME
                // _playerInputActions.Gameplay.Jump.canceled -= ctx => InteractReleased(); //PRESS AND RELEASE

                _playerInputActions.Gameplay.Movement.performed -= PlayerMove;
                _playerInputActions.Gameplay.Movement.canceled -= PlayerStopMoving;


                _playerInputActions.Gameplay.Interact.performed -= ctx => AttackLeft();
                _playerInputActions.Gameplay.Interact.canceled -= ctx => UndoAttackLeft();

                _playerInputActions.Gameplay.PauseGame.performed -= ctx => EscapeButtonPressed();
                _playerInputActions.Debug.EscapeKey.performed -= ctx => DebugEscapeButtonPressed();
            }
        }

        private void DebugEscapeButtonPressed()
        {
            OnDebugEscPressed.Invoke();
        }


        private void EscapeButtonPressed()
        {
            OnDebugEscPressed.Invoke();
        }

        private void UndoAttackLeft()
        {
            OnPlayerReleaseLeftButtonMouse.Invoke();
        }

        private void PlayerStopMoving(InputAction.CallbackContext obj)
        {
            this.MovementInGameAxis = Vector3.zero;
        }

        private void PlayerMove(InputAction.CallbackContext obj)
        {
            if (!(obj.ReadValue<Vector2>().magnitude > 0.01f)) return;
            Vector2 vectorReceived = obj.ReadValue<Vector2>();
            MovementInGameAxis = new Vector3(vectorReceived.x, 0, vectorReceived.y);
            OnPlayerStartMoving.Invoke();
        }

        private void AttackLeft()
        {
            OnPlayerPressLeftButtonMouse.Invoke();
        }

        private void PlayerPressSpace()
        {
            Debug.Log("PRESS ONE TIME");

            OnPlayerPressedSpaceKey.Invoke();
        }

        public void DisableGameplayInput()
        {
            _playerInputActions.Gameplay.Disable();
        }

        public void EnableGameplayInput()
        {
            _playerInputActions.Gameplay.Enable();
        }
    }
}