﻿using System;
using Presentation.Enemy.IA;
using UnityEngine;
using UnityEngine.AI;

namespace Presentation.Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        private IPatrol _patrolMovement;
        private bool _canMove;
        private Vector3 _direction;
        private float _movementSpeed;
        public event Action<Vector3> OnEnemyChangesDirection = delegate { };

        void Start()
        {
            _canMove = true;
        }


        public Vector3 VectorOfMovement => _direction;

        public void SetMovementSpeed(float movementSpeed)
        {
            _movementSpeed = movementSpeed;
            _navMeshAgent.speed = _movementSpeed;

        }

        private void SetPositionToGo(Transform positionToGo)
        {
            _direction = positionToGo.position - transform.position;

            OnEnemyChangesDirection.Invoke(_direction);
        }

        public void StartPatrolling()
        {
            var initialPatrolPoint = _patrolMovement.GetCurrentPatrolPoint();
            SetPositionToGo(initialPatrolPoint);
        }

        public void SetPatrolLogic(IPatrol patrolMovement)
        {
            _patrolMovement = patrolMovement;
        }

        public void GoToPosition(Transform getCurrentPatrolPoint)
        {
          bool setDestination = _navMeshAgent.SetDestination(getCurrentPatrolPoint.position);
          Debug.Log($"ENEMY {gameObject.name} CAN REACH {setDestination} "); 
        }

        public void ChangePatrolPointSet(int objIdPatrolPointToChange)
        {
            _patrolMovement.ChangePatrolPointSet(objIdPatrolPointToChange);
        }
    }
}