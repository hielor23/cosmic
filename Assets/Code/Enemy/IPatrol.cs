﻿using UnityEngine;

namespace Presentation.Enemy.IA
{
    public interface IPatrol
    {
        bool IsNearObjectivePatrolPoint();
        void UpdatePatrolPoint();
        Transform GetCurrentPatrolPoint();
        void ChangePatrolPointSet(int changePatrolSetToChange);
        bool PatrolPointIsObjectivePatrolPoint();
    }
}