﻿using System;
using UnityEngine;

namespace Presentation.Enemy.IA
{
    public class ObjectDetector : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToDetect;
        [SerializeField] private float _distanceToCatchPlayer;
        [SerializeField] private bool _detectObjects;
        [SerializeField] private FieldOfView3D _fieldOfView3D;
        private bool _objectFound = false;

        public event Action<GameObject> OnSearchedObjectFound = delegate { };

        public GameObject ObjectToSearch => _objectToDetect;

        public float DistanceToCatchPlayer => _distanceToCatchPlayer;

        private void Start()
        {
            _fieldOfView3D.OnHitSomething += HasHitSomething;
            _fieldOfView3D.OnResetHitItem += HasLostItem;
        }

        private void HasLostItem()
        {
            _objectFound = false;
        }

        private void HasHitSomething(Transform obj)
        {
            if (!_detectObjects)
            {
                return;
            }

            _objectFound = true;
            OnSearchedObjectFound.Invoke(obj.gameObject);
        }
        
        public bool CanSeeObject()
        {
            return _objectFound;
        }
        
        private void OnDrawGizmos()
        {
            Debug.DrawRay(transform.position, transform.forward * (_distanceToCatchPlayer - 0.5f), Color.red);
        }
    }
}