﻿using System.Collections.Generic;

public interface ILanguageManager
{
    LanguageText GetActualLanguageText();
    LanguagesKeys GetActualLanguageKey();
    string GetLanguageNameByKey(LanguagesKeys languagesKeyToChange);
    List<LanguageName> GetLanguageNames();
    void SetActualLanguageText(LanguagesKeys languagesKeyToChange);

}