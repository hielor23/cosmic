﻿using Domain.JsonTranslator;
using UnityEngine;
using Utils;

namespace Domain.LoaderSaveGame
{
    public class LoadWithPlayerPrefs : ILoader
    {
        private IJsonator _jsonator;

        public LoadWithPlayerPrefs()
        {
            _jsonator = ServiceLocator.Instance.GetService<IJsonator>();
        }

        public SaveGameData LoadGame()
        {
            if (!PlayerPrefs.HasKey("SaveGame"))
            {
                return null;
            }

            var dataToFromJson = PlayerPrefs.GetString("SaveGame");
            SaveGameData saveGameDataJson = _jsonator.FromJson<SaveGameData>(dataToFromJson);
            return saveGameDataJson;
        }

        public bool HasSavedGame()
        {
            return PlayerPrefs.GetInt("HasSavedGame") == 1;
        }
        
    }
}