﻿using System;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;

public class TutorialInfoViewLevelSeven : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _infoAttackEnemyInfoText;


    [SerializeField] private Transform _objectAttackCube, _positionToMoveAttackCube;


    [SerializeField] private float _durationToDoDownSurviveLevel;


    private ILanguageManager _languageManager;
    private IEventManager _eventManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowSurviveInfoTutorialSignal>(delegate
        {
            UtilsMovements.MoveObjectToPositionWithDuration(_objectAttackCube, _positionToMoveAttackCube,
                _durationToDoDownSurviveLevel);
        });
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ShowSurviveInfoTutorialSignal>(delegate
        {
            UtilsMovements.MoveObjectToPositionWithDuration(_objectAttackCube, _positionToMoveAttackCube,
                _durationToDoDownSurviveLevel);
        });
    }


    private void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        _infoAttackEnemyInfoText.SetText(getActualLanguageText.InfoHowToAttackEnemyInGameSceneKey);
    }
}