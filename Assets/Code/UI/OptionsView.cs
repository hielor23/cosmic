﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _audioText, _languageText, _clearSaveGameText, _backText, _titleText;
    [SerializeField] private Button _clearSaveGameButton, _backButton;
    [SerializeField] private Toggle _toggleAudio;
    [SerializeField] private TMP_Dropdown _dropdown;
    [SerializeField] private GameDeletedPopUpView _gameDeletedPopUpView;

    public event Action OnBackButtonPressed, OnDeleteSaveGamePressed;

    void Start()
    {
        _gameDeletedPopUpView.gameObject.SetActive(false);
        _gameDeletedPopUpView.OnOkButtonPressed += OkButtonDeletedPopUp;

        _dropdown.onValueChanged.AddListener(DropdownChanged);
        _toggleAudio.onValueChanged.AddListener(ToggleChanged);
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());
        _clearSaveGameButton.onClick.AddListener(HandleClearSaveGameDelete);
    }

    private void OkButtonDeletedPopUp()
    {
        _gameDeletedPopUpView.gameObject.SetActive(false);
    }

    
    private void HandleClearSaveGameDelete()
    {
        OnDeleteSaveGamePressed.Invoke();
        _gameDeletedPopUpView.gameObject.SetActive(true);
    }

    private void ToggleChanged(bool arg0)
    {
        // throw new NotImplementedException();
    }

    private void DropdownChanged(int arg0)
    {
        // throw new NotImplementedException();
    }


    public void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.OptionsTitleInitScene);
        _audioText.SetText(languageText.AudioTextInitScene);
        _languageText.SetText(languageText.LanguageTextInitScene);
        _clearSaveGameText.SetText(languageText.ClearSaveGameButtonInitScene);
        _backText.SetText(languageText.BackButtonInitScene);
    }

    public void SetDropdownValues(List<LanguageName> languageNames, LanguagesKeys currentLanguageKey)
    {
        _dropdown.options.Clear ();
        int selectedLanguage = 0;
        for (var index = 0; index < languageNames.Count; index++)
        {
            var languageName = languageNames[index];
            if (languageName.ActualLanguageKey == currentLanguageKey)
            {
                selectedLanguage = index;
            }

            // Debug.Log(languageName.LanguageTitle);
            _dropdown.options.Add(new TMP_Dropdown.OptionData() {text = languageName.LanguageTitle});
        }

        _dropdown.value = selectedLanguage;
    }
}