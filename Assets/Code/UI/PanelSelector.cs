﻿using UnityEngine;

public class PanelSelector : MonoBehaviour
{
    public GameObject optionsPanel, creditsPanel, initPanel, actualPanel;

    private void Start()
    {
        actualPanel = initPanel;
    }
    public void OptionsPanel() 
    {
        optionsPanel.SetActive(true);
        actualPanel = optionsPanel;
        initPanel.SetActive(false);
    }

    public void CreditsPanel() 
    {
        creditsPanel.SetActive(true);
        actualPanel = creditsPanel;
        initPanel.SetActive(false);
    }

    public void Back() 
    {
        initPanel.SetActive(true);
        actualPanel.SetActive(false);
        actualPanel = initPanel;
    }

    public void CloseApp() 
    {
        Application.Quit();
    }

}
