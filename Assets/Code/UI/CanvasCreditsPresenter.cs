﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class CanvasCreditsPresenter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _buttonExitText, _title, _programmersTitle, _gameDesignerTitle, _artistVFXTitle, _compositorsTitle, _artist3DTitle, artist2DTitle;
    [SerializeField] private Button _exitButton;

    private SceneChanger _sceneChanger;
    private ILanguageManager _languageManager;
    private void Awake()
    {
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _exitButton.onClick.AddListener(()=>_sceneChanger.GoToInitScene());
    }

    private void Start()
    {
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void OnDestroy()
    {
        _exitButton.onClick.RemoveListener(()=>_sceneChanger.GoToInitScene());
    }

    private void SetLanguageTranslations(LanguageText languageText)
    {
        _title.SetText(languageText.TitleCreditsSceneCreditsKey);
        _programmersTitle.SetText(languageText.ProgrammersTitleCreditsKey);
        _artistVFXTitle.SetText(languageText.ArtistVFXTitleCreditsKey);
        _compositorsTitle.SetText(languageText.CompositorsVFXTitleCreditsKey);
        _artist3DTitle.SetText(languageText.Artist3DTitleCreditsKey);
        artist2DTitle.SetText(languageText.Artist2DTitleCreditsKey);
        _gameDesignerTitle.SetText(languageText.GameDesignerTitleCreditsKey);
        _buttonExitText.SetText(languageText.ExitButtonSceneCreditsKey);
    }
}
