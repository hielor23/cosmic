﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InitSceneView : MonoBehaviour
{
    [SerializeField] private Button _playModeButton,
        _showCharactersHistoryButton,
        _storeButton,
        _optionsButton,
        _creditsButton,
        _exitButton;

    [SerializeField] private TextMeshProUGUI _playModeButtonText,
        _showCharactersButtonText,
        _storeButtonText,
        _optionsButtonText,
        _creditsButtonText,
        _exitButtonText;

    public event Action OnPlayButtonPressed,
        OnShowCharactersHistoryButtonPressed,
        OnStoreButtonPressed,
        OnCreditsButtonPressed,
        OnOptionsButtonPressed,
        OnExitButtonPressed;

    void Start()
    {
        _playModeButton.onClick.AddListener(() => OnPlayButtonPressed.Invoke());
        _showCharactersHistoryButton.onClick.AddListener(() => OnShowCharactersHistoryButtonPressed.Invoke());
        _storeButton.onClick.AddListener(() => OnStoreButtonPressed.Invoke());
        _creditsButton.onClick.AddListener(() => OnCreditsButtonPressed.Invoke());
        _optionsButton.onClick.AddListener(() => OnOptionsButtonPressed.Invoke());
        _exitButton.onClick.AddListener(() => OnExitButtonPressed.Invoke());
    }

    private void OnDestroy()
    {
        _playModeButton.onClick.RemoveListener(() => OnPlayButtonPressed.Invoke());
        _showCharactersHistoryButton.onClick.RemoveListener(() => OnShowCharactersHistoryButtonPressed.Invoke());
        _storeButton.onClick.RemoveListener(() => OnStoreButtonPressed.Invoke());
        _creditsButton.onClick.RemoveListener(() => OnCreditsButtonPressed.Invoke());
        _optionsButton.onClick.RemoveListener(() => OnOptionsButtonPressed.Invoke());
        _exitButton.onClick.RemoveListener(() => OnExitButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _playModeButtonText.SetText(languageText.PlayButtonInitScene);
        _showCharactersButtonText.SetText(languageText.ShowCharactersHistoryButtonInitScene);
        _creditsButtonText.SetText(languageText.CreditsButtonInitScene);
        _optionsButtonText.SetText(languageText.OptionsButtonInitScene);
        _exitButtonText.SetText(languageText.ExitButtonInitScene);
        _storeButtonText.SetText(languageText.StoreButtonInitScene);
    }
}