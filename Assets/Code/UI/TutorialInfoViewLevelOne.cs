﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using Utils;

//Move & Rotate

public class TutorialInfoViewLevelOne : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _infoMoveText,
        _infoRotateText;

    [SerializeField] private Transform _objectWSMove,
        _objectADMove,
        positionToMoveObjectWs,
        _positionToMoveObjectAd;


    [SerializeField] private float _durationToDoDownMovementWS,
        _durationToDoDownMovementAD;

    private Tweener _tweenerWS;
    private ILanguageManager _languageManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
    }

    private void Start()
    {
        SetLanguageTranslations(_languageManager.GetActualLanguageText());
        ShowInfoTutorial();
    }

    private void SetLanguageTranslations(LanguageText getActualLanguageText)
    {
        _infoMoveText.SetText(getActualLanguageText.InfoHowToMoveInGameSceneKey);
        _infoRotateText.SetText(getActualLanguageText.InfoHowToRotateInGameSceneKey);
    }

    private void ShowInfoTutorial()
    {
        _objectADMove.gameObject.SetActive(false);
        _tweenerWS = UtilsMovements.MoveObjectToPositionWithDuration(_objectWSMove, positionToMoveObjectWs,
            _durationToDoDownMovementWS);
        UtilsMovements.MoveObjectToPositionWithDuration(_objectADMove, _positionToMoveObjectAd,
            _durationToDoDownMovementAD);
        _tweenerWS.onComplete += ActivateADInfo;
    }

    private void ActivateADInfo()
    {
        _tweenerWS.onComplete -= ActivateADInfo;
        _objectADMove.gameObject.SetActive(true);
    }
}