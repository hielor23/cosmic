﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverView : MonoBehaviour
{
    [SerializeField] private Button _restartButton, _exitButton;
    [SerializeField] private TextMeshProUGUI _restartButtonText, _exitButtonText, _titleMenuText;

    public event Action OnRestartGame = delegate { };
    public event Action OnExitLevel = delegate { };

    private void Start()
    {
        _restartButton.onClick.AddListener(() => OnRestartGame.Invoke());
        _exitButton.onClick.AddListener(() => OnExitLevel.Invoke());
    }

    private void OnDestroy()
    {
        _restartButton.onClick.RemoveListener(() => OnRestartGame.Invoke());
        _exitButton.onClick.RemoveListener(() => OnExitLevel.Invoke());
    }

    public void SetLanguageTranslations(LanguageText languageText)
    {
        _restartButtonText.SetText(languageText.RestartButtonMenuInGameKey);
        _exitButtonText.SetText(languageText.ExitButtonMenuInGameKey);
        _titleMenuText.SetText(languageText.TitleGameOverMenuInGameKey);
    }
}