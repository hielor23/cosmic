﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuView : MonoBehaviour
{
    [SerializeField] private Button _continueButton,_restartButton, _exitButton;
    [SerializeField] private TextMeshProUGUI _continueButtonText, _restartButtonText, _exitButtonText, _titleMenuText;

    public event Action OnRestartGame = delegate { };
    public event Action OnResumeGame = delegate { };
    public event Action OnExitLevel = delegate { };

    private void Start()
    {
        _continueButton.onClick.AddListener(() => OnResumeGame.Invoke());
        _restartButton.onClick.AddListener(() => OnRestartGame.Invoke());
        _exitButton.onClick.AddListener(() => OnExitLevel.Invoke());
    }

    private void OnDestroy()
    {
        _continueButton.onClick.RemoveListener(() => OnResumeGame.Invoke());
        _restartButton.onClick.RemoveListener(() => OnRestartGame.Invoke());
        _exitButton.onClick.RemoveListener(() => OnExitLevel.Invoke());
    }

    public void SetLanguageTranslations(LanguageText languageText)
    {
        _continueButtonText.SetText(languageText.ResumeButtonPauseMenuInGameKey);
        _restartButtonText.SetText(languageText.RestartButtonMenuInGameKey);
        _exitButtonText.SetText(languageText.ExitButtonMenuInGameKey);
        _titleMenuText.SetText(languageText.TitlePauseMenuInGameKey);
    }
}