﻿using UnityEngine;
using Utils;

public class CanvasInitScenePresenter : MonoBehaviour
{
    [SerializeField] private InitSceneView _initSceneView;
    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private CreditsView _creditsView;
    [SerializeField] private StoreView _storeView;
    [SerializeField] private HistoryCharactersView _historyCharacters;

    private RetrievePlayerProgression _retrievePlayerProgression;
    private UpdatePlayerProgression _updatePlayerProgression;
    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;

    void Awake()
    {
        _retrievePlayerProgression = ServiceLocator.Instance.GetService<RetrievePlayerProgression>();
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _updatePlayerProgression = ServiceLocator.Instance.GetService<UpdatePlayerProgression>();
    }

    void Start()
    {
        ShowInitView();
        _optionsView.gameObject.SetActive(false);
        _creditsView.gameObject.SetActive(false);
        _storeView.gameObject.SetActive(false);
        _historyCharacters.gameObject.SetActive(false);

        _optionsView.OnBackButtonPressed += HandleOptionsCharactersBackButton;
        _optionsView.OnDeleteSaveGamePressed += DeleteSaveGame;

        _creditsView.OnBackButtonPressed += HandleCreditsBackButton;
        _storeView.OnBackButtonPressed += HandleHistoryCharactersBackButton;
        _historyCharacters.OnBackButtonPressed += HandleHistoryCharactersBackButton;

        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _initSceneView.OnOptionsButtonPressed += ShowOptions;
        _initSceneView.OnCreditsButtonPressed += ShowCredits;
        _initSceneView.OnPlayButtonPressed += InitPlayMode;
        _initSceneView.OnExitButtonPressed += ExitGame;
        _initSceneView.OnStoreButtonPressed += ShowStore;
        _initSceneView.OnShowCharactersHistoryButtonPressed += ShowCharactersHistory;
        // _initSceneView.OnArcadeModeButtonPressed += InitArcadeMode;
    }


    private void HandleOptionsCharactersBackButton()
    {
        _optionsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleCreditsBackButton()
    {
        _creditsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleHistoryCharactersBackButton()
    {
        _historyCharacters.gameObject.SetActive(false);
        ShowInitView();
    }

    private void ShowStore()
    {
        new PlayClickButtonSignal().Execute();

        _storeView.gameObject.SetActive(false);
        _storeView.SetLanguageStrings(_languageManager.GetActualLanguageText());

        ShowInitView();
    }


    private void ShowInitView()
    {
        new PlayClickButtonSignal().Execute();

        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());

        _initSceneView.gameObject.SetActive(true);
    }

    private void HideInitView()
    {
        _initSceneView.gameObject.SetActive(false);
    }

    private void ShowCharactersHistory()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _historyCharacters.SetLanguageStrings(_languageManager.GetActualLanguageText());

        _historyCharacters.gameObject.SetActive(true);
    }

    private void DeleteSaveGame()
    {
        new PlayClickButtonSignal().Execute();

        _updatePlayerProgression.DeletePlayerProgress();
    }


    private void ExitGame()
    {
        new PlayClickButtonSignal().Execute();

        Application.Quit();
    }

    private void InitPlayMode()
    {
        new PlayClickButtonSignal().Execute();

        if (!ExistsPreviousSaveGame())
        {
            _sceneChanger.GoToFirstLevel();
            return;
        }

        _sceneChanger.LoadCurrentLevelFromInitScene(_retrievePlayerProgression.GameData.LastLevelCompleted);
    }

    private void ShowCredits()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _creditsView.SetLanguageStrings(_languageManager.GetActualLanguageText());

        _creditsView.gameObject.SetActive(true);
    }

    private void ShowOptions()
    {
        new PlayClickButtonSignal().Execute();

        HideInitView();
        _optionsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _optionsView.SetDropdownValues(_languageManager.GetLanguageNames(), _languageManager.GetActualLanguageKey());
        _optionsView.gameObject.SetActive(true);
    }

    private bool ExistsPreviousSaveGame()
    {
        return _retrievePlayerProgression.GameData.LastLevelCompleted > 0;
    }
}