﻿using System;
using UnityEngine;

public class PortalCollider : MonoBehaviour
{
    public event Action OnPlayerHitPortalCollider = delegate { };
    private bool _playerHasEntered;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("Player")  || _playerHasEntered) return;
        _playerHasEntered = true;
        OnPlayerHitPortalCollider.Invoke();
        new PlayerHasCompletedLevelSignal().Execute();
    }
}
