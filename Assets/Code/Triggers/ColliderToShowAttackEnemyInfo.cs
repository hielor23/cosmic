﻿using Presentation.Player;
using UnityEngine;

public class ColliderToShowAttackEnemyInfo : MonoBehaviour
{
    private bool _hasBeenExecutedBefore;

    private void OnTriggerEnter(Collider other)
    {
        var playerFacade = other.GetComponent<PlayerFacade>();
        if (!playerFacade || _hasBeenExecutedBefore) return;
        _hasBeenExecutedBefore = true;
        new ShowAttackEnemyInfoTutorialSignal().Execute();
    }
}