﻿using UnityEngine;
using Utils.Input;


public class PlayerMovement : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;
    [SerializeField] private float _standingMoveSpeed, _crouchMoveSpeed, _jumpForce, _rotationSpeed;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private GroundChecker _groundChecker;
    [SerializeField] private Animator _animator;
    [SerializeField] private PlayerColliders playerColliders;
    private float _currentSpeed, _colliderOriginalSize;
    private bool _jumpPressed, _canMove, _crotch;
    private Vector3 _jumpMovement;
    private static readonly int MovementZ = Animator.StringToHash("MovementZ");
    private static readonly int MovementX = Animator.StringToHash("MovementX");
    private static readonly int Crouch = Animator.StringToHash("Crouch");
    private static readonly int Speed = Animator.StringToHash("Speed");

    private Vector3 _movementDirection;
    private Quaternion _rotation;
    // public Vector3 MovementDirection => _movementDirection;
    // public Quaternion GetRotation => _rotation;

    void Start()
    {
        Cursor.visible = false;
        _canMove = true;
        _currentSpeed = _standingMoveSpeed;
    }

    public void Init(ReadInputPlayer readInputPlayer)
    {
        _readInputPlayer = readInputPlayer;
        _readInputPlayer.OnPlayerPressedSpaceKey += EnableJump;
        _readInputPlayer.OnPlayerPressedControlKey += EnableCrouch;
        _readInputPlayer.OnPlayerReleaseControlKey += DisableCrouch;
    }

    private void DisableCrouch()
    {
        _crotch = false;
        _currentSpeed = _standingMoveSpeed;
        playerColliders.DisableCrouchCollider();
    }

    private void EnableCrouch()
    {
        _crotch = true;
        _currentSpeed = _crouchMoveSpeed;
        playerColliders.EnableCrouchCollider();
    }

    private void EnableJump()
    {
        if (!_groundChecker.IsGround())
        {
            return;
        }

        _jumpPressed = true;
    }

    void FixedUpdate()
    {
        if (!_canMove)
        {
            return;
        }

        Vector3 input = _readInputPlayer.MovementInGameAxis;
        // Debug.Log($"INPUT {input} {_crotch} {_jumpPressed}");
        _jumpMovement = Vector3.zero;

        if (_jumpPressed && !_crotch)
        {
            // Debug.Log($"JUMP{_jumpPressed}");
            _jumpMovement = Vector3.up * _jumpForce;
            _jumpPressed = false;
            _rigidbody.AddForce(_jumpMovement, ForceMode.Force);
            new PlayPlayerJumpSoundSignal().Execute();
        }

        if (!_jumpPressed && _crotch)
        {
            // Debug.Log("CROTCH");
            _animator.SetBool(Crouch, true);
        }

        if (!_crotch)
        {
            _animator.SetBool(Crouch, false);
        }

        _animator.SetFloat(MovementZ, input.z);
        _animator.SetFloat(Speed, input.magnitude);
        // _animator.SetFloat(MovementX, input.x);
        _rotation = Quaternion.Euler(_rigidbody.rotation.eulerAngles + Vector3.up * input.x * _rotationSpeed);
        _rigidbody.rotation = _rotation;
        // if (!PlayerIsMoving(input))
        // {
        //     return;
        // }

        Vector3 movement = transform.forward * input.z;
        movement.y = 0;
        _movementDirection = movement;
        _rigidbody.position += movement * Time.deltaTime * _currentSpeed;
    }


    public void EnableMovement()
    {
        _canMove = true;
    }

    public void DisableMovement()
    {
        _canMove = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawRay(transform.position, _movementDirection);
    }
}