﻿using UnityEngine;

public class PlayerColliders : MonoBehaviour
{
    [SerializeField] private Collider _crouchCollider, _standCollider;

    private void Start()
    {
        DisableCrouchCollider();
    }

    public void EnableCrouchCollider()
    {
        _crouchCollider.enabled = true;
        _standCollider.enabled = false;
    }

    public void DisableCrouchCollider()
    {
        _crouchCollider.enabled = false;
        _standCollider.enabled = true;
    }
}