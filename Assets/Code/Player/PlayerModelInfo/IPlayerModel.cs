﻿namespace App.PlayerModelInfo
{
    public interface IPlayerModel
    {
        void ResetData();
    }
}