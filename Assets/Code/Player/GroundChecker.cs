﻿using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    [SerializeField] private LayerMask _layerMaskToJump;
    [SerializeField] private float _distanceRaycast;


    public bool IsGround()
    {
        RaycastHit raycastHit;
        Physics.Raycast(transform.position, Vector3.down, out raycastHit, _distanceRaycast, _layerMaskToJump);
        return raycastHit.collider;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * _distanceRaycast);
    }
}