﻿using Presentation.Items;
using UnityEngine;

public class Cube : PickableItem, IExecuteAction
{
    [SerializeField] private BoxCollider _boxCollider;
    [SerializeField] private ActionToDo _actionToDoWhenIsInPlace;
    [SerializeField] private float _timeToWaitToExecuteAction, _timeToDissolveCube;
    private float _distanceToObject;
    private bool _canMove, _playerIsInteractingWithCube;
    private Timer _timer;

    private Renderer _renderer;

    private void Start()
    {
        _timer = new Timer();
        _timer.SetTimeToWait(_timeToWaitToExecuteAction);

        _playerIsInteractingWithCube = false;
        _distanceToObject = -1;
        _canMove = true;
        _renderer = GetComponent<Renderer>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider) return;
        BoxChecker boxChecker = other.collider.GetComponent<BoxChecker>();
        if (boxChecker) return;
        _canMove = false;
    }


    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        _playerIsInteractingWithCube = true;
        transform.parent = objectWhichInteract.transform;
    }


    public override void StopInteraction()
    {
        Debug.Log("DEJAMOS CUBO");
        _distanceToObject = -1;
        _canMove = true;
        _playerIsInteractingWithCube = false;
        transform.parent = null;
    }

    public override bool PlayerIsInteractingWithIt()
    {
        return _playerIsInteractingWithCube;
    }

    public void ExecuteAction()
    {
        Debug.Log("EXECUTE");
        _timer.OnTimerEnds += Dissolve;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void Dissolve()
    {
        _timer.OnTimerEnds -= Dissolve;
        _boxCollider.enabled = false;
        DissolveInfoCube dissolveInfoCube = new DissolveInfoCube()
        {
            Renderer = _renderer,
            TimeToDissolveCube = _timeToDissolveCube
        };

        _actionToDoWhenIsInPlace.Init(dissolveInfoCube);
        _actionToDoWhenIsInPlace.ExecuteAction();
        new PlayCubeVanishSoundSignal().Execute();
    }
}