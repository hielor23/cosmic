﻿using DG.Tweening;
using UnityEngine;
using VFX;

public abstract class DoorControlBase : MonoBehaviour
{
    [SerializeField] protected MeshRenderer _doorRight, _doorLeft;
    [SerializeField] protected PortalCollider _portalEffect;
    [SerializeField]  protected float timeToDisolveDoorsEffect;
    protected bool IsActivated;

    private Sequence _dissolveDoors;

    protected void ActivateDoor()
    {
        if(IsActivated) return;
        ObjectEffect.Singleton.OneTimeEffect(_doorRight, ObjectEffect.Singleton.Dissolve, timeToDisolveDoorsEffect);
        _dissolveDoors = ObjectEffect.Singleton.OneTimeEffect(_doorLeft, ObjectEffect.Singleton.Dissolve, timeToDisolveDoorsEffect);
        _dissolveDoors.onComplete += ActivatePortal;
        IsActivated = true;
        new PlayDoorActivateSoundSignal().Execute();
    }

    private void ActivatePortal()
    {
        _dissolveDoors.onComplete -= ActivateDoor;
        _portalEffect.gameObject.SetActive(true);
        LevelEffect.Singleton.OneTimeEffect(_portalEffect.GetComponent<MeshRenderer>(), LevelEffect.Singleton.OpenDoor, 0);
    }

    protected void OnPlayerEnter()
    {
        new PlayerHasCompletedLevelSignal().Execute();
    }
}