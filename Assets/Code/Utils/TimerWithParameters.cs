﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerParameters
{
    public List<GameObject> GameObject;
    public TimerWithParameters Timer { get; set; }

    public TimerParameters()
    {
        GameObject = new List<GameObject>();
    }
}
public class TimerWithParameters : IEnumerator
{
    public event Action<TimerParameters> OnTimerEnds = delegate { };
    private float _timeToWait;

    public void SetTimeToWait(float time)
    {
        _timeToWait = time;
    }

    public IEnumerator TimerCoroutine(TimerParameters parameters)
    {
        parameters.Timer = this;
        yield return new WaitForSeconds(_timeToWait);
        OnTimerEnds.Invoke(parameters);
    }

    public bool MoveNext()
    {
        throw new NotImplementedException();
    }

    public void Reset()
    {
        throw new NotImplementedException();
    }

    public object Current { get; }
}