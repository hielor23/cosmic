﻿using System;
using System.Collections.Generic;
using Signals.EventManager;
using Object = System.Object;

public class EventManager : IEventManager
{
    private readonly Dictionary<SubscriberData, Action<Object>> _subscribersActionsToDo;

    public EventManager()
    {
        _subscribersActionsToDo = new Dictionary<SubscriberData, Action<Object>>();
    }

    public void AddActionToSignal<T>(Action<T> actionToDo)
    {
        //TODO DO AN RANDOM IDENTIFIER
        Action<object> wrapperCallback = args => actionToDo((T) args);

        Add(typeof(T), wrapperCallback);
    }

    public void NotifySubscribers<T>(T signal)
    {
        Notify(signal);
    }

    public void RemoveActionFromSignal<T>(Action<T> action)
    {
        Action<object> wrapperCallback = args => action((T) args);
        RemoveAction(typeof(T), wrapperCallback);
    }

    private void Add(Type type, Action<Object> actionToDo)
    {
        SubscriberData data = new SubscriberData() {SignalType = type, Action = actionToDo};

        if (_subscribersActionsToDo.ContainsKey(data))
        {
            return;
        }

        _subscribersActionsToDo.Add(data, actionToDo);
    }


    private void Notify(Object signal)
    {

        foreach (var VARIABLE in _subscribersActionsToDo.Keys)
        {
            if (VARIABLE.SignalType == signal.GetType())
            {
                _subscribersActionsToDo[VARIABLE].Invoke(signal);
            }
        }
    }

    private void RemoveAction(Type type, Action<Object> actionToDo)
    {
        SubscriberData data = new SubscriberData() {SignalType = type, Action = actionToDo};

        _subscribersActionsToDo.Remove(data);
    }


}