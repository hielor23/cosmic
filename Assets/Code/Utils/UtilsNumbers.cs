﻿using System;

public class UtilsNumbers
{
    public static float Truncate(float value, int digits)
    {
        double mult = Math.Pow(10.0, digits);
        double result = Math.Truncate(mult * value) / mult;
        return (float) result;
    }
}