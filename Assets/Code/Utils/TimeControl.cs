﻿using System;
using UnityEngine;

public class TimeControl : MonoBehaviour
{
    [SerializeField] private int _maxLevelTime;
    private float _currentTime;
    private bool timeFinished = false;

    public event Action OnTimeHasPast = delegate { };

    public bool TimeFinished => timeFinished;

    void Start()
    {
        _currentTime = _maxLevelTime;
    }

    public float GetTime()
    {
        return _currentTime;
    }

    void Update()
    {
        if (timeFinished) return;
        CalculateTime();
        if (_currentTime >= 0) return;
        timeFinished = true;
        OnTimeHasPast.Invoke();
    }

    private void CalculateTime()
    {
        _currentTime -= 1 * Time.deltaTime;
    }
}