﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameDeletedPopUpView : MonoBehaviour
{
    public event Action OnOkButtonPressed;
    [SerializeField] private Button _okButton;
    [SerializeField] private TextMeshProUGUI _title, _info, _buttonText;
    void Start()
    {
        _okButton.onClick.AddListener(() => OnOkButtonPressed.Invoke());

    }
    
    private void OnDestroy()
    {
        _okButton.onClick.RemoveListener(() => OnOkButtonPressed.Invoke());

    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _title.SetText(languageText.TitleDeletedSaveGameInitScene);
        _info.SetText(languageText.InfoDeletedSaveGameInitScene);
        _buttonText.SetText(languageText.OkButtonInitScene);
    }
}
